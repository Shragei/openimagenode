#include <imageio.h>
#include <node.h>
#include <v8.h>
#include "OINImageSpec.h"
#include "OINTypeDesc.h"

using namespace v8;
using namespace node;

Persistent<FunctionTemplate> oinImageSpec::constructor;

oinImageSpec::oinImageSpec(){

}

oinImageSpec::~oinImageSpec(){
}

void oinImageSpec::Initialize(Handle<Object> exports){
  Local<FunctionTemplate> tpl = FunctionTemplate::New(New);
  tpl->SetClassName(String::NewSymbol("ImageSpec"));
  tpl->InstanceTemplate()->SetInternalFieldCount(1);
  
  Local<ObjectTemplate> proto = tpl->PrototypeTemplate();
//  proto->SetNamedPropertyHandler(catchAllGet,catchAllSet);
  proto->SetAccessor(String::NewSymbol("X"),IntGet,IntSet);
  proto->SetAccessor(String::NewSymbol("Y"),IntGet,IntSet);
  proto->SetAccessor(String::NewSymbol("Z"),IntGet,IntSet);
  
  proto->SetAccessor(String::NewSymbol("Width"),IntGet,IntSet);
  proto->SetAccessor(String::NewSymbol("Height"),IntGet,IntSet);
  proto->SetAccessor(String::NewSymbol("Depth"),IntGet,IntSet);
  
  proto->SetAccessor(String::NewSymbol("DisplayX"),IntGet,IntSet);
  proto->SetAccessor(String::NewSymbol("DisplayY"),IntGet,IntSet);
  proto->SetAccessor(String::NewSymbol("DisplayZ"),IntGet,IntSet);
  
  proto->SetAccessor(String::NewSymbol("DisplayWidth"),IntGet,IntSet);
  proto->SetAccessor(String::NewSymbol("DisplayHeight"),IntGet,IntSet);
  proto->SetAccessor(String::NewSymbol("DisplayDepth"),IntGet,IntSet);
  
  proto->SetAccessor(String::NewSymbol("TileWidth"),IntGet,IntSet);
  proto->SetAccessor(String::NewSymbol("TileHeight"),IntGet,IntSet);
  proto->SetAccessor(String::NewSymbol("TileDepth"),IntGet,IntSet);
  
  proto->SetAccessor(String::NewSymbol("ChannelCount"),IntGet,IntSet);
  proto->SetAccessor(String::NewSymbol("AlphaChannel"),IntGet,IntSet);
  proto->SetAccessor(String::NewSymbol("ZChannel"),IntGet,IntSet);
  
  proto->SetAccessor(String::NewSymbol("HasDeep"),BoolGet,BoolSet);
  
  proto->SetAccessor(String::NewSymbol("TypeDesc"),TypeDescGet,TypeDescSet);
  
  constructor=Persistent<FunctionTemplate>::New(tpl);
  exports->Set(String::NewSymbol("ImageSpec"),tpl->GetFunction());
}

Handle<Value> oinImageSpec::New(const Arguments& args){
  HandleScope scope;
  oinImageSpec *is=new oinImageSpec();
  is->Wrap(args.This());
  return scope.Close(args.This());
}
Handle<Value> oinImageSpec::catchAllGet(Local<String> name,const AccessorInfo &info){
  HandleScope scope;
  String::AsciiValue attrib(name);
  printf("%s\n",*attrib);
  return scope.Close(Integer::New(0));
}

Handle<Value> oinImageSpec::catchAllSet(Local<String> name,Local<Value> value,const AccessorInfo &info){
  HandleScope scope;
  
  return scope.Close(Integer::New(0));
}
Handle<Value> oinImageSpec::IntGet(Local<String> name,const AccessorInfo &info){
  HandleScope scope;
  oinImageSpec* oinIS=ObjectWrap::Unwrap<oinImageSpec>(info.This());
  String::AsciiValue attrib(name);
  int val;

  if(!strcmp(*attrib,"X")){
    val=oinIS->is.x;
  }else if(!strcmp(*attrib,"Y")){
    val=oinIS->is.y;
  }else if(!strcmp(*attrib,"Z")){
    val=oinIS->is.z;
  }else if(!strcmp(*attrib,"Width")){
    val=oinIS->is.width;
  }else if(!strcmp(*attrib,"Height")){
    val=oinIS->is.height;
  }else if(!strcmp(*attrib,"Depth")){
    val=oinIS->is.depth;
  }else if(!strcmp(*attrib,"DisplayX")){
    val=oinIS->is.full_x;
  }else if(!strcmp(*attrib,"DisplayY")){
    val=oinIS->is.full_y;
  }else if(!strcmp(*attrib,"DisplayZ")){
    val=oinIS->is.full_z;
  }else if(!strcmp(*attrib,"DisplayWidth")){
    val=oinIS->is.full_width;
  }else if(!strcmp(*attrib,"DisplayHeight")){
    val=oinIS->is.full_height;
  }else if(!strcmp(*attrib,"DisplayDepth")){
    val=oinIS->is.full_depth;
  }else if(!strcmp(*attrib,"TileWidth")){
    val=oinIS->is.tile_width;
  }else if(!strcmp(*attrib,"TileHeight")){
    val=oinIS->is.tile_height;
  }else if(!strcmp(*attrib,"TileDepth")){
    val=oinIS->is.tile_depth;
  }else if(!strcmp(*attrib,"ChannelCount")){
    val=oinIS->is.tile_depth;
  }else if(!strcmp(*attrib,"AlphaChannel")){
    val=oinIS->is.alpha_channel;
  }else if(!strcmp(*attrib,"ZChannel")){
    val=oinIS->is.z_channel;
  }
  
  return scope.Close(Integer::New(val));
}


void oinImageSpec::IntSet(Local<String> name,Local<Value> value,const AccessorInfo &info){
  oinImageSpec* oinIS=ObjectWrap::Unwrap<oinImageSpec>(info.This());

  String::AsciiValue attrib(name);
  int val=value->Int32Value();
  
  if(!strcmp(*attrib,"X")){
    oinIS->is.x=val;
  }else if(!strcmp(*attrib,"Y")){
    oinIS->is.y=val;
  }else if(!strcmp(*attrib,"Z")){
    oinIS->is.z=val;
  }else if(!strcmp(*attrib,"Width")){
    oinIS->is.width=val;
  }else if(!strcmp(*attrib,"Height")){
    oinIS->is.height=val;
  }else if(!strcmp(*attrib,"Depth")){
    oinIS->is.depth=val;
  }else if(!strcmp(*attrib,"DisplayX")){
    oinIS->is.full_x=val;
  }else if(!strcmp(*attrib,"DisplayY")){
    oinIS->is.full_y=val;
  }else if(!strcmp(*attrib,"DisplayZ")){
    oinIS->is.full_z=val;
  }else if(!strcmp(*attrib,"DisplayWidth")){
    oinIS->is.full_width=val;
  }else if(!strcmp(*attrib,"DisplayHeight")){
    oinIS->is.full_height=val;
  }else if(!strcmp(*attrib,"DisplayDepth")){
    oinIS->is.full_depth=val;
  }else if(!strcmp(*attrib,"TileWidth")){
    oinIS->is.tile_width=val;
  }else if(!strcmp(*attrib,"TileHeight")){
    oinIS->is.tile_height=val;
  }else if(!strcmp(*attrib,"TileDepth")){
    oinIS->is.tile_depth=val;
  }else if(!strcmp(*attrib,"ChannelCount")){
    oinIS->is.tile_depth=val;
  }else if(!strcmp(*attrib,"AlphaChannel")){
    oinIS->is.alpha_channel=val;
  }else if(!strcmp(*attrib,"ZChannel")){
    oinIS->is.z_channel=val;
  }
}

Handle<Value> oinImageSpec::BoolGet(Local<String> name,const AccessorInfo &info){
  HandleScope scope;
  oinImageSpec* oinIS=ObjectWrap::Unwrap<oinImageSpec>(info.This());
  String::AsciiValue attrib(name);
  bool val;
  
  if(!strcmp(*attrib,"HasDeep")){
    val=oinIS->is.deep;
  }
  
  return scope.Close(Boolean::New(val));
}

void oinImageSpec::BoolSet(Local<String> name,Local<Value> value,const AccessorInfo &info){
  oinImageSpec* oinIS=ObjectWrap::Unwrap<oinImageSpec>(info.This());

  String::AsciiValue attrib(name);
  bool val=value->BooleanValue();
  if(!strcmp(*attrib,"HasDeep")){
    oinIS->is.deep=val;
  }
}

Handle<Value> oinImageSpec::TypeDescGet(Local<String> name,const AccessorInfo &info){
  HandleScope scope;
  oinImageSpec* oinSpec=ObjectWrap::Unwrap<oinImageSpec>(info.This());

  Local<Object> typeObj=oinTypeDesc::constructor->GetFunction()->NewInstance();
  oinTypeDesc* oinType=ObjectWrap::Unwrap<oinTypeDesc>(typeObj);
  
  oinType->td=oinSpec->is.format;

  return scope.Close(typeObj);
}

void oinImageSpec::TypeDescSet(Local<String> name,Local<Value> value,const AccessorInfo &info){
  oinImageSpec* oinSpec=ObjectWrap::Unwrap<oinImageSpec>(info.This());
  oinTypeDesc* oinType=ObjectWrap::Unwrap<oinTypeDesc>(value->ToObject());
  
  oinSpec->is.format=oinType->td;
}
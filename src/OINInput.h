#ifndef OINInput_H
#define OINInput_H

#include <v8.h>
#include <node.h>
#include <imageio.h>

class oinInput : public node::ObjectWrap{
  public:
    oinInput(std::string&);
    ~oinInput();
    static void Initialize(v8::Handle<v8::Object> exports);
    OpenImageIO::ImageInput* img;
  private:
    
  
    static v8::Handle<v8::Value> New(const v8::Arguments& args);
    static v8::Handle<v8::Value> FormatName(const v8::Arguments& args);
    static v8::Handle<v8::Value> Spec(v8::Local<v8::String> name,const v8::AccessorInfo &info);
//    static v8::Handle<v8::Value> Supports(const v8::FunctionCallbackInfo<v8::Value>& args);
    static v8::Handle<v8::Value> CurrentSubimage(const v8::Arguments& args);
    static v8::Handle<v8::Value> CurrentMiplevel(const v8::Arguments& args);
    static v8::Handle<v8::Value> SeekSubimage(const v8::Arguments& args);
    static v8::Handle<v8::Value> ReadScanline(const v8::Arguments& args);
    static v8::Handle<v8::Value> ReadTile(const v8::Arguments& args);
    static v8::Handle<v8::Value> ReadImage(const v8::Arguments& args);
    
    static v8::Persistent<v8::Function> constructor;
};
#endif
#include <imageio.h>
#include <node.h>
#include <v8.h>

#include "OINTypeDesc.h"
#include "OINImageSpec.h"
#include "OINInput.h"
#include "OINOutput.h"

using namespace v8;
using namespace OpenImageIO;
Handle<Value> Open(const Arguments& args){//TODO fill in skeleton
  HandleScope scope;
  
  return scope.Close(Undefined());
}

Handle<Value> Write(const Arguments& args){//TODO fill in skeleton
  HandleScope scope;
  
  return scope.Close(Undefined());
}

Handle<Value> GetError(const Arguments& args){
  HandleScope scope;
  
  return scope.Close(String::New(OpenImageIO::geterror().c_str()));
}

Handle<Value> SetAttribute(const Arguments& args){
  HandleScope scope;
  
  if(args.Length()<2){
    ThrowException(Exception::TypeError(String::New("Attribute requires 2 arguments (name, value)")));
    return scope.Close(Undefined());
  }
  
  if(!args[0]->IsString()){
    ThrowException(Exception::TypeError(String::New("Attribute requires first argument to be string")));
    return scope.Close(Undefined());
  }
  
  String::AsciiValue name(args[0]);
  bool ret=false;
  
  if(args[1]->IsInt32()){
    int t=args[1]->Int32Value();
    ret=OpenImageIO::attribute(*name,TypeDesc::TypeInt,&t);
  }else if(args[1]->IsNumber()){
    float t=args[1]->NumberValue();
    ret=OpenImageIO::attribute(*name,TypeDesc::TypeFloat,&t);
  }else if(args[1]->IsString()){
    String::AsciiValue v8str(args[1]);
    
    ret=OpenImageIO::attribute(*name,TypeDesc::TypeString,*v8str);
  }else{
    ThrowException(Exception::TypeError(String::New("Attribute requires second argument to be number or string")));
  }
  
  return scope.Close(Boolean::New(ret));
}

Handle<Value> GetAttribute(const Arguments& args){//TODO fill in skeleton
  HandleScope scope;

  
  return scope.Close(Undefined());
}

Handle<Value> ConvertImage(const Arguments& args){//TODO fill in skeleton
  HandleScope scope;

  
  return scope.Close(Undefined());
}

void Initialize(Handle<Object> exports){
  oinTypeDesc::Initialize(exports);
  oinImageSpec::Initialize(exports);
  oinInput::Initialize(exports);
  oinOutput::Initialize(exports);
  
/*  exports->Set(String::NewSymbol("Open"),
    FunctionTemplate::New(Open)->GetFunction());
  exports->Set(String::NewSymbol("Write"),
    FunctionTemplate::New(Write)->GetFunction());
  */  
  exports->Set(String::NewSymbol("Version"),
    Number::New(OpenImageIO::openimageio_version()));
  exports->Set(String::NewSymbol("GetError"),
    FunctionTemplate::New(GetError)->GetFunction());
    
  exports->Set(String::NewSymbol("SetAttribute"),
    FunctionTemplate::New(SetAttribute)->GetFunction());
  exports->Set(String::NewSymbol("GetAttribute"),
    FunctionTemplate::New(GetAttribute)->GetFunction());
    
  exports->Set(String::NewSymbol("ConvertImage"),
    FunctionTemplate::New(ConvertImage)->GetFunction());

}



NODE_MODULE(OpenImageNode,Initialize);
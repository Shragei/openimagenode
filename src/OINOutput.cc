#include <node.h>
#include <node_buffer.h>
#include "OINOutput.h"
#include "OINTypeDesc.h"
#include "OINImageSpec.h"

using namespace v8;
using namespace node;

Persistent<Function> oinOutput::constructor;

oinOutput::oinOutput(std::string &path,OpenImageIO::ImageSpec &spec){
  img=OpenImageIO::ImageOutput::create(path);
  img->open(path,spec);
}

oinOutput::~oinOutput(){
  img->close();
  delete(img);
}

void oinOutput::Initialize(Handle<Object> exports){
  Local<FunctionTemplate> tpl = FunctionTemplate::New(New);
  tpl->SetClassName(String::NewSymbol("Output"));
  tpl->InstanceTemplate()->SetInternalFieldCount(1);
  
  Local<ObjectTemplate> proto = tpl->PrototypeTemplate();
  
  proto->Set(String::NewSymbol("FormatName"),
      FunctionTemplate::New(FormatName)->GetFunction());
  proto->Set(String::NewSymbol("Supports"),
      FunctionTemplate::New(Supports)->GetFunction());
  proto->Set(String::NewSymbol("WriteScanline"),
      FunctionTemplate::New(WriteScanline)->GetFunction());
  proto->Set(String::NewSymbol("WriteTile"),
      FunctionTemplate::New(WriteTile)->GetFunction());
  proto->Set(String::NewSymbol("WriteImage"),
      FunctionTemplate::New(WriteImage)->GetFunction());
  
  constructor=Persistent<Function>::New(tpl->GetFunction());
  exports->Set(String::NewSymbol("Output"),constructor);
}

Handle<Value> oinOutput::New(const Arguments& args){
  HandleScope scope;
  oinImageSpec *spec;
  
  if(!(args.Length()>=1&&args[0]->IsString())){
    ThrowException(Exception::TypeError(String::New("Output requires first argument to be string")));
    return scope.Close(Undefined());
  }
  
  String::AsciiValue v8str(args[0]);
  std::string path(*v8str);
  
  Local<Object> tmp=args[1]->ToObject();
  if(oinImageSpec::constructor->HasInstance(tmp)){
    spec=ObjectWrap::Unwrap<oinImageSpec>(tmp);
  }else{
    ThrowException(Exception::TypeError(String::New("Output requires second argument to be imageSpec")));
    return scope.Close(Undefined());
  }
  
  oinOutput* output = new oinOutput(path,spec->is);
  
  if(!output||!output->img){
    printf("%s\n",OpenImageIO::geterror().c_str());
    return scope.Close(Undefined());
  }
  
  output->Wrap(args.This());
  return scope.Close(args.This());
}

Handle<Value> oinOutput::Supports(const Arguments& args){
  HandleScope scope;
  
  oinOutput* output=ObjectWrap::Unwrap<oinOutput>(args.This());
  String::AsciiValue feature(args[0]->ToString());
  
  bool ret=output->img->supports(std::string(*feature));

  return scope.Close(Boolean::New(ret));
}

Handle<Value> oinOutput::FormatName(const Arguments& args){
  HandleScope scope;
  
  oinOutput* output=ObjectWrap::Unwrap<oinOutput>(args.This());
  const char *format=output->img->format_name();

  return scope.Close(String::New(format));
}
/*
Handle<Value> oinOutput::Spec(Local<String> name,const AccessorInfo &info){
  HandleScope scope;
  oinOutput* oinImg=ObjectWrap::Unwrap<oinOutput>(info.This());

  Local<Object> specObj=oinImageSpec::constructor->GetFunction()->NewInstance();
  oinImageSpec* oinSpec=ObjectWrap::Unwrap<oinImageSpec>(specObj);
  
  oinSpec->is=oinImg->img->spec();

  return scope.Close(specObj);
}
*/
Handle<Value> oinOutput::WriteScanline(const Arguments& args){
  HandleScope scope;
  int y=0;
  int z=0;
  OpenImageIO::ImageSpec spec;
  oinTypeDesc* oinTD;
  char *buff;
  
  oinOutput* output=ObjectWrap::Unwrap<oinOutput>(args.This());
  
  if(!args[0]->IsNumber()){
    ThrowException(Exception::TypeError(String::New("WriteScanline requires first argument to be an interger")));
    return scope.Close(Undefined());
  }
  y=args[0]->Int32Value();

  if(args[1]->IsNumber()){
    z=args[1]->Int32Value();
  }else{
    Local<Object> tmp=args[1]->ToObject();
    if(oinTypeDesc::constructor->HasInstance(tmp)){
      oinTD=ObjectWrap::Unwrap<oinTypeDesc>(tmp);
      buff=Buffer::Data(args[2]);
    }
  }
  
  if(args.Length()==3){
    Local<Object> tmp=args[2]->ToObject();
    if(oinTypeDesc::constructor->HasInstance(tmp)){
      oinTD=ObjectWrap::Unwrap<oinTypeDesc>(tmp);
      buff=Buffer::Data(args[3]);
    }
  }
  
  if(!oinTD){
    ThrowException(Exception::TypeError(String::New("WriteScanline requires TypeDesc to be supplied")));
    return scope.Close(Undefined());
  }

  bool ret=output->img->write_scanline(y,z,oinTD->td,buff);
  
  return scope.Close(Boolean::New(ret));
}

Handle<Value> oinOutput::WriteTile(const Arguments& args){
  HandleScope scope;
  int x=0;
  int y=0;
  int z=0;
  OpenImageIO::ImageSpec spec;
  oinTypeDesc* oinTD;
  char *buff;
  
  oinOutput* output=ObjectWrap::Unwrap<oinOutput>(args.This());
  
  if(!args[0]->IsNumber()){
    ThrowException(Exception::TypeError(String::New("WriteScanline requires first argument to be an interger")));
    return scope.Close(Undefined());
  }
  x=args[0]->Int32Value();
  
  if(!args[1]->IsNumber()){
    ThrowException(Exception::TypeError(String::New("WriteTile requires second argument to be an interger")));
    return scope.Close(Undefined());
  }
  y=args[1]->Int32Value();
  
  if(args[2]->IsNumber()){
    z=args[2]->Int32Value();
  }else{
    Local<Object> tmp=args[2]->ToObject();
    if(oinTypeDesc::constructor->HasInstance(tmp)){
      oinTD=ObjectWrap::Unwrap<oinTypeDesc>(tmp);
      buff=Buffer::Data(args[3]);
    }
  }
  
  if(args.Length()==3){
    Local<Object> tmp=args[3]->ToObject();
    if(oinTypeDesc::constructor->HasInstance(tmp)){
      oinTD=ObjectWrap::Unwrap<oinTypeDesc>(tmp);
      buff=Buffer::Data(args[4]);
    }
  }
  
  if(!oinTD){
    ThrowException(Exception::TypeError(String::New("WriteTile requires TypeDesc to be supplied")));
    return scope.Close(Undefined());
  }

  bool ret=output->img->write_tile(x,y,z,oinTD->td,buff);
  
  return scope.Close(Boolean::New(ret));
}


Handle<Value> oinOutput::WriteImage(const Arguments& args){
  HandleScope scope;
  oinTypeDesc* oinTD;
  char *buff;
  
  oinOutput* output=ObjectWrap::Unwrap<oinOutput>(args.This());
  
  buff=Buffer::Data(args[0]);
  
  Local<Object> tmp=args[1]->ToObject();
  if(!oinTypeDesc::constructor->HasInstance(tmp)){
    ThrowException(Exception::TypeError(String::New("WriteImage requires second argument to be TypeDesc")));
    return scope.Close(Undefined());
  }
  oinTD=ObjectWrap::Unwrap<oinTypeDesc>(tmp);

  bool ret=output->img->write_image(oinTD->td,buff);
  
  return scope.Close(Boolean::New(ret));
}
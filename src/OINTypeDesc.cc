#include <imageio.h>
#include <node.h>
#include <v8.h>
#include "OINTypeDesc.h"

using namespace v8;
using namespace node;

Persistent<FunctionTemplate> oinTypeDesc::constructor;

oinTypeDesc::oinTypeDesc(){

}

oinTypeDesc::~oinTypeDesc(){
}

void oinTypeDesc::Initialize(Handle<Object> exports){
  Local<FunctionTemplate> tpl = FunctionTemplate::New(New);
  tpl->SetClassName(String::NewSymbol("TypeDesc"));
  tpl->InstanceTemplate()->SetInternalFieldCount(1);
  
  Local<ObjectTemplate> proto = tpl->PrototypeTemplate();
  
  proto->SetAccessor(String::NewSymbol("Size"),Size);
  proto->SetAccessor(String::NewSymbol("Base"),BaseTypeGet,BaseTypeSet);
  proto->SetAccessor(String::NewSymbol("Aggregate"),AggregateTypeGet,AggregateTypeSet);
  proto->SetAccessor(String::NewSymbol("Vecsemantics"),VecsemanticsTypeGet,VecsemanticsTypeSet);
  
  proto->SetAccessor(String::NewSymbol("ElementSize"),ElementSize);
  proto->SetAccessor(String::NewSymbol("BaseSize"),BaseSize);
  proto->SetAccessor(String::NewSymbol("IsFloatingPoint"),IsFloatingPoint);
  
  proto->Set(String::NewSymbol("FromString"),
      FunctionTemplate::New(FromString)->GetFunction());
  proto->Set(String::NewSymbol("ToString"),
      FunctionTemplate::New(ToString)->GetFunction());    
      
  constructor=Persistent<FunctionTemplate>::New(tpl);
  exports->Set(String::NewSymbol("TypeDesc"),tpl->GetFunction());
}

Handle<Value> oinTypeDesc::New(const Arguments& args){
  HandleScope scope;
  oinTypeDesc *td=new oinTypeDesc();

  td->Wrap(args.This());
  return scope.Close(args.This());
}

Handle<Value> oinTypeDesc::Size(Local<String> name,const AccessorInfo &info){
  HandleScope scope;
  oinTypeDesc* oinTD=ObjectWrap::Unwrap<oinTypeDesc>(info.This());

  return scope.Close(Integer::New(oinTD->td.size()));
}


Handle<Value> oinTypeDesc::BaseTypeGet(Local<String> name,const AccessorInfo &info){
  HandleScope scope;
  const char *typeStr;
  oinTypeDesc* oinTD=ObjectWrap::Unwrap<oinTypeDesc>(info.This());
  
  switch(oinTD->td.basetype){
    case OpenImageIO::TypeDesc::UNKNOWN:
      typeStr="UNKNOWN";
	  break;
	case OpenImageIO::TypeDesc::NONE:
      typeStr="NONE";
	  break;
	case OpenImageIO::TypeDesc::UCHAR:
      typeStr="UCHAR";
	  break;
	case OpenImageIO::TypeDesc::CHAR:
      typeStr="CHAR";
	  break;
	case OpenImageIO::TypeDesc::USHORT:
      typeStr="USHORT";
	  break;
	case OpenImageIO::TypeDesc::SHORT:
      typeStr="SHORT";
	  break;
	case OpenImageIO::TypeDesc::UINT:
      typeStr="UINT";
	  break;
	case OpenImageIO::TypeDesc::INT:
      typeStr="INT";
	  break;
	case OpenImageIO::TypeDesc::ULONGLONG:
      typeStr="ULONGLONG";
	  break;
	case OpenImageIO::TypeDesc::LONGLONG:
      typeStr="LONGLONG";
	  break;
	case OpenImageIO::TypeDesc::HALF:
      typeStr="HALF";
	  break;
	case OpenImageIO::TypeDesc::FLOAT:
      typeStr="FLOAT";
	  break;
	case OpenImageIO::TypeDesc::DOUBLE:
      typeStr="DOUBLE";
	  break;
	case OpenImageIO::TypeDesc::STRING:
      typeStr="STRING";
	  break;
	case OpenImageIO::TypeDesc::PTR:
      typeStr="PTR";
	  break;
	default:
      typeStr="";
	 
  }
  return scope.Close(String::NewSymbol(typeStr));
}
void oinTypeDesc::BaseTypeSet(Local<String> name,Local<Value> value,const AccessorInfo &info){
  oinTypeDesc* oinTD=ObjectWrap::Unwrap<oinTypeDesc>(info.This());
  String::AsciiValue v8str(value);
  
  const char *typeStr[15]={"UNKNOWN","NONE","UCHAR","CHAR","USHORT","SHORT", \
                         "UINT","INT","ULONGLONG","LONGLONG","HALF","FLOAT", \
                         "DOUBLE","STRING","PTR"};
  const char typedat[15]={OpenImageIO::TypeDesc::UNKNOWN,OpenImageIO::TypeDesc::NONE, \
                             OpenImageIO::TypeDesc::UCHAR,OpenImageIO::TypeDesc::CHAR, \
                             OpenImageIO::TypeDesc::USHORT,OpenImageIO::TypeDesc::SHORT, \
                             OpenImageIO::TypeDesc::UINT,OpenImageIO::TypeDesc::INT, \
                             OpenImageIO::TypeDesc::ULONGLONG,OpenImageIO::TypeDesc::LONGLONG, \
                             OpenImageIO::TypeDesc::HALF,OpenImageIO::TypeDesc::FLOAT, \
                             OpenImageIO::TypeDesc::DOUBLE,OpenImageIO::TypeDesc::STRING, \
                             OpenImageIO::TypeDesc::PTR};
                             
  int i;
  bool set=false;
  
  for(i=0;i<15;i++){
    if(strcmp(typeStr[i],*v8str)==0){
      oinTD->td.basetype=typedat[i];
      set=true;
      break;
    }
  }
  //If type of format is not supported set it to unknown
  if(!set)
    oinTD->td.basetype=OpenImageIO::TypeDesc::UNKNOWN;
}

Handle<Value> oinTypeDesc::AggregateTypeGet(Local<String> name,const AccessorInfo &info){
  HandleScope scope;
  const char *typeStr;
  oinTypeDesc* oinTD=ObjectWrap::Unwrap<oinTypeDesc>(info.This());
  switch(oinTD->td.aggregate){
    case OpenImageIO::TypeDesc::SCALAR:
      typeStr="SCALAR";
	  break;
	case OpenImageIO::TypeDesc::VEC2:
      typeStr="VEC2";
	  break;
	case OpenImageIO::TypeDesc::VEC3:
      typeStr="VEC3";
	  break;
	case OpenImageIO::TypeDesc::VEC4:
      typeStr="VEC4";
	  break;
	case OpenImageIO::TypeDesc::MATRIX44:
      typeStr="MATRIX44";
	  break;
	default:
      typeStr="";
	 
  }
  return scope.Close(String::NewSymbol(typeStr));
}

void oinTypeDesc::AggregateTypeSet(Local<String> name,Local<Value> value,const AccessorInfo &info){
  oinTypeDesc* oinTD=ObjectWrap::Unwrap<oinTypeDesc>(info.This());
  String::AsciiValue v8str(value);
  
  const char *typeStr[5]={"SCALAR","VEC2","VEC3","VEC4","MATRIX44"};
  const char typedat[5]={OpenImageIO::TypeDesc::SCALAR,OpenImageIO::TypeDesc::VEC2, \
                             OpenImageIO::TypeDesc::VEC3,OpenImageIO::TypeDesc::VEC4, \
                             OpenImageIO::TypeDesc::MATRIX44};
                             
  int i;
  bool set=false;
  
  for(i=0;i<5;i++){
    if(strcmp(typeStr[i],*v8str)==0){
      oinTD->td.aggregate=typedat[i];
      set=true;
      break;
    }
  }
  //If type of format is not supported set it to scalar
  if(!set)
    oinTD->td.aggregate=OpenImageIO::TypeDesc::SCALAR;
}

Handle<Value> oinTypeDesc::VecsemanticsTypeGet(Local<String> name,const AccessorInfo &info){
  HandleScope scope;
  const char *typeStr;
  oinTypeDesc* oinTD=ObjectWrap::Unwrap<oinTypeDesc>(info.This());
  switch(oinTD->td.vecsemantics){
    case OpenImageIO::TypeDesc::NOSEMANTICS:
      typeStr="NOSEMANTICS";
	  break;
	case OpenImageIO::TypeDesc::COLOR:
      typeStr="COLOR";
	  break;
	case OpenImageIO::TypeDesc::POINT:
      typeStr="POINT";
	  break;
	case OpenImageIO::TypeDesc::VECTOR:
      typeStr="VECTOR";
	  break;
	case OpenImageIO::TypeDesc::NORMAL:
      typeStr="NORMAL";
	  break;
  case OpenImageIO::TypeDesc::TIMECODE:
    typeStr="TIMECODE";
  break;
  case OpenImageIO::TypeDesc::KEYCODE:
    typeStr="KEYCODE";
  break;
	default:
      typeStr="";
	 
  }
  return scope.Close(String::NewSymbol(typeStr));
}

void oinTypeDesc::VecsemanticsTypeSet(Local<String> name,Local<Value> value,const AccessorInfo &info){
  oinTypeDesc* oinTD=ObjectWrap::Unwrap<oinTypeDesc>(info.This());
  String::AsciiValue v8str(value);
  
  const char *typeStr[7]={"NOSEMANTICS","COLOR","POINT","VECTOR","NORMAL","TIMECODE","KEYCODE"};
  const char typedat[7]={OpenImageIO::TypeDesc::NOSEMANTICS,OpenImageIO::TypeDesc::COLOR, \
                             OpenImageIO::TypeDesc::POINT,OpenImageIO::TypeDesc::VECTOR, \
                             OpenImageIO::TypeDesc::NORMAL,OpenImageIO::TypeDesc::TIMECODE, \
                             OpenImageIO::TypeDesc::KEYCODE};
                             
  int i;
  bool set=false;
  
  for(i=0;i<7;i++){
    if(strcmp(typeStr[i],*v8str)==0){
      oinTD->td.vecsemantics=typedat[i];
      set=true;
      break;
    }
  }
  //If type of format is not supported set it to scalar
  if(!set)
    oinTD->td.vecsemantics=OpenImageIO::TypeDesc::NOSEMANTICS;
}

Handle<Value> oinTypeDesc::ElementSize(Local<String> name,const AccessorInfo &info){
  HandleScope scope;
  oinTypeDesc* oinTD=ObjectWrap::Unwrap<oinTypeDesc>(info.This());

  return scope.Close(Integer::New(oinTD->td.elementsize()));
}

Handle<Value> oinTypeDesc::BaseSize(Local<String> name,const AccessorInfo &info){
  HandleScope scope;
  oinTypeDesc* oinTD=ObjectWrap::Unwrap<oinTypeDesc>(info.This());

  return scope.Close(Integer::New(oinTD->td.basesize()));
}

Handle<Value> oinTypeDesc::IsFloatingPoint(Local<String> name,const AccessorInfo &info){
  HandleScope scope;
  oinTypeDesc* oinTD=ObjectWrap::Unwrap<oinTypeDesc>(info.This());
  bool isfloat=false;
  int t=oinTD->td.basetype;
  
  if(t==OpenImageIO::TypeDesc::HALF||
     t==OpenImageIO::TypeDesc::FLOAT||
     t==OpenImageIO::TypeDesc::DOUBLE)
    isfloat=true;
  
  return scope.Close(Boolean::New(isfloat));
}

Handle<Value> oinTypeDesc::FromString(const Arguments& args){
  HandleScope scope;
  oinTypeDesc* oinTD=ObjectWrap::Unwrap<oinTypeDesc>(args.This());
  if(!args[0]->IsString()){
    ThrowException(Exception::TypeError(String::New("FromString requires first argument to be string")));
    return scope.Close(Undefined());
  }
  String::AsciiValue v8str(args[0]);
  
  size_t len=oinTD->td.fromstring(*v8str);
  return scope.Close(Boolean::New(len>0));
}

#ifndef OINOutput_H
#define OINOutput_H

#include <v8.h>
#include <node.h>
#include <imageio.h>

class oinOutput : public node::ObjectWrap{
  public:
    oinOutput(std::string &path,OpenImageIO::ImageSpec &spec);
    ~oinOutput();
    static void Initialize(v8::Handle<v8::Object> exports);
    OpenImageIO::ImageOutput* img;
  private:
    
  
    static v8::Handle<v8::Value> New(const v8::Arguments& args);
    static v8::Handle<v8::Value> FormatName(const v8::Arguments& args);
    static v8::Handle<v8::Value> Supports(const v8::Arguments& args);
    static v8::Handle<v8::Value> WriteScanline(const v8::Arguments& args);
    static v8::Handle<v8::Value> WriteTile(const v8::Arguments& args);
    static v8::Handle<v8::Value> WriteImage(const v8::Arguments& args);
    
    static v8::Persistent<v8::Function> constructor;
};
#endif
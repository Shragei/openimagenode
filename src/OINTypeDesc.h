#ifndef OINTypeDesc_H
#define OINTypeDesc_H

#include <v8.h>
#include <node.h>
#include <imageio.h>
#include <typedesc.h>

class oinTypeDesc : public node::ObjectWrap{
  public:
    static v8::Persistent<v8::FunctionTemplate> constructor;
    static void Initialize(v8::Handle<v8::Object> exports);
	  OpenImageIO::TypeDesc td;
	
  private:
    explicit oinTypeDesc();
    ~oinTypeDesc();
    
    static v8::Handle<v8::Value> New(const v8::Arguments& args);
    
    static v8::Handle<v8::Value> Size(v8::Local<v8::String> name,const v8::AccessorInfo &info);
    
    static v8::Handle<v8::Value> BaseTypeGet(v8::Local<v8::String> name,const v8::AccessorInfo &info);
    static void BaseTypeSet(v8::Local<v8::String> name,v8::Local<v8::Value> value,const v8::AccessorInfo &info);
    static v8::Handle<v8::Value> AggregateTypeGet(v8::Local<v8::String> name,const v8::AccessorInfo &info);
    static void AggregateTypeSet(v8::Local<v8::String> name,v8::Local<v8::Value> value,const v8::AccessorInfo &info);
    static v8::Handle<v8::Value> VecsemanticsTypeGet(v8::Local<v8::String> name,const v8::AccessorInfo &info);
    static void VecsemanticsTypeSet(v8::Local<v8::String> name,v8::Local<v8::Value> value,const v8::AccessorInfo &info);
    
    static v8::Handle<v8::Value> ElementSize(v8::Local<v8::String> name,const v8::AccessorInfo &info);
    static v8::Handle<v8::Value> BaseSize(v8::Local<v8::String> name,const v8::AccessorInfo &info);
    static v8::Handle<v8::Value> IsFloatingPoint(v8::Local<v8::String> name,const v8::AccessorInfo &info);
    static v8::Handle<v8::Value> FromString(const v8::Arguments& args);

 //   static v8::Handle<v8::Value> ToString(const v8::FunctionCallbackInfo<v8::Value>& args);
};
#endif
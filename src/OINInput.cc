#include <node.h>
#include <node_buffer.h>
#include "OINInput.h"
#include "OINTypeDesc.h"
#include "OINImageSpec.h"

using namespace v8;
using namespace node;

Local<Object> attachBuffer(Buffer *buff){
  v8::Local<v8::Object> globalObj=v8::Context::GetCurrent()->Global();
  v8::Local<v8::Function> bufferConstructor=v8::Local<v8::Function>::Cast(globalObj->Get(v8::String::New("Buffer")));
  v8::Handle<v8::Value> constructorArgs[3]={buff->handle_, \
                                            v8::Integer::New(Buffer::Length(buff->handle_)), \
                                            v8::Integer::New(0)};

  return bufferConstructor->NewInstance(3,constructorArgs);
}

oinInput::oinInput(std::string &path){
  img=OpenImageIO::ImageInput::open(path);
}

oinInput::~oinInput(){
  img->close();
  delete(img);
}

Persistent<Function> oinInput::constructor;

void oinInput::Initialize(Handle<Object> exports){
  Local<FunctionTemplate> tpl = FunctionTemplate::New(New);
  tpl->SetClassName(String::NewSymbol("Input"));
  tpl->InstanceTemplate()->SetInternalFieldCount(1);
  
  Local<ObjectTemplate> proto = tpl->PrototypeTemplate();

  proto->SetAccessor(String::NewSymbol("Spec"),Spec);
  
  proto->Set(String::NewSymbol("FormatName"),
      FunctionTemplate::New(FormatName)->GetFunction());
//  proto->Set(String::NewSymbol("Supports"),
//      FunctionTemplate::New(Supports)->GetFunction());
  proto->Set(String::NewSymbol("CurrentSubimage"),
      FunctionTemplate::New(CurrentSubimage)->GetFunction());
  proto->Set(String::NewSymbol("CurrentMiplevel"),
      FunctionTemplate::New(CurrentMiplevel)->GetFunction());
  proto->Set(String::NewSymbol("SeekSubimage"),
      FunctionTemplate::New(SeekSubimage)->GetFunction());
  proto->Set(String::NewSymbol("ReadScanline"),
      FunctionTemplate::New(ReadScanline)->GetFunction());
  proto->Set(String::NewSymbol("ReadTile"),
      FunctionTemplate::New(ReadTile)->GetFunction());
  proto->Set(String::NewSymbol("ReadImage"),
      FunctionTemplate::New(ReadImage)->GetFunction());
  
  constructor=Persistent<Function>::New(tpl->GetFunction());
  exports->Set(String::NewSymbol("Input"),constructor);
}

Handle<Value> oinInput::New(const Arguments& args){
  HandleScope scope;
  
  if(!(args.Length()>=1&&args[0]->IsString())){
    ThrowException(Exception::TypeError(String::New("Input requires first argument to be string")));
    return scope.Close(Undefined());
  }
  String::AsciiValue v8str(args[0]);
  std::string tmp(*v8str);
  oinInput* input = new oinInput(tmp);
  
  if(!input||!input->img){
    ThrowException(Exception::TypeError(String::New(OpenImageIO::geterror().c_str())));
    return scope.Close(Undefined());
  }
  
  input->Wrap(args.This());
  return scope.Close(args.This());
}

Handle<Value> oinInput::Spec(Local<String> name,const AccessorInfo &info){
  HandleScope scope;
  oinInput* oinImg=ObjectWrap::Unwrap<oinInput>(info.This());

  Local<Object> specObj=oinImageSpec::constructor->GetFunction()->NewInstance();
  oinImageSpec* oinSpec=ObjectWrap::Unwrap<oinImageSpec>(specObj);
  
  oinSpec->is=oinImg->img->spec();

  return scope.Close(specObj);
}

Handle<Value> oinInput::FormatName(const Arguments& args){
  HandleScope scope;
  
  oinInput* input=ObjectWrap::Unwrap<oinInput>(args.This());
  const char *format=input->img->format_name();

  return scope.Close(String::New(format));
}

/*
Handle<Value> oinInput::Supports(const Arguments& args){
  HandleScope scope;

  return scope.Close(Undefined());
}
*/

Handle<Value> oinInput::CurrentSubimage(const Arguments& args){
  HandleScope scope;
  
  oinInput* input=ObjectWrap::Unwrap<oinInput>(args.This());
  int cur=input->img->current_subimage();
  
  return scope.Close(Integer::New(cur));
}

Handle<Value> oinInput::CurrentMiplevel(const Arguments& args){
  HandleScope scope;
  
  oinInput* input=ObjectWrap::Unwrap<oinInput>(args.This());
  int cur=input->img->current_miplevel();
  
  return scope.Close(Integer::New(cur));
}

Handle<Value> oinInput::SeekSubimage(const Arguments& args){
  HandleScope scope;
  OpenImageIO::ImageSpec tmpImageSpec;
  int subimage=0;
  int miplevel=0;
  
  oinInput* input=ObjectWrap::Unwrap<oinInput>(args.This());
  
  if(!args[0]->IsNumber()){
    ThrowException(Exception::TypeError(String::New("SeekSubimage requires first argument to be an interger")));
    return scope.Close(Undefined());
  }
  subimage=args[0]->Int32Value();

  if(args.Length()>1){
    if(!args[1]->IsNumber()){
      ThrowException(Exception::TypeError(String::New("SeekSubimage requires second argument to be an interger")));
      return scope.Close(Undefined());
    }
    miplevel=args[1]->Int32Value();
  }

  
  bool ret=input->img->seek_subimage(subimage,miplevel,tmpImageSpec);
  
  return scope.Close(Boolean::New(ret));
}
//static Buffer* New(const char *data, size_t len);
Handle<Value> oinInput::ReadScanline(const Arguments& args){
  HandleScope scope;
  int y=0;
  int z=0;
  size_t size;
  OpenImageIO::ImageSpec spec;
  oinTypeDesc* oinTD;
  
  oinInput* input=ObjectWrap::Unwrap<oinInput>(args.This());
  
  if(!args[0]->IsNumber()){
    ThrowException(Exception::TypeError(String::New("ReadScanline requires first argument to be an interger")));
    return scope.Close(Undefined());
  }
  y=args[0]->Int32Value();

  if(args[1]->IsNumber()){
    z=args[1]->Int32Value();
  }else{
    Local<Object> tmp=args[1]->ToObject();
    if(oinTypeDesc::constructor->HasInstance(tmp))
      oinTD=ObjectWrap::Unwrap<oinTypeDesc>(tmp);
  }
  
  if(args.Length()==3){
    Local<Object> tmp=args[2]->ToObject();
    if(oinTypeDesc::constructor->HasInstance(tmp))
      oinTD=ObjectWrap::Unwrap<oinTypeDesc>(tmp);
  }
  
  if(!oinTD){
    ThrowException(Exception::TypeError(String::New("ReadScanline requires TypeDesc to be supplied")));
    return scope.Close(Undefined());
  }
  
  spec=input->img->spec();
  
  size=spec.width*spec.nchannels*oinTD->td.size();
  Buffer* line=Buffer::New(size);

  input->img->read_scanline(y,z,oinTD->td,Buffer::Data(line));
  
  return scope.Close(attachBuffer(line));
}

v8::Handle<v8::Value> oinInput::ReadTile(const v8::Arguments& args){
  HandleScope scope;
  int x=0;
  int y=0;
  int z=0;
  size_t size;
  OpenImageIO::ImageSpec spec;
  oinTypeDesc* oinTD;

  oinInput* input=ObjectWrap::Unwrap<oinInput>(args.This());
  
  if(!args[0]->IsNumber()){
    ThrowException(Exception::TypeError(String::New("ReadTile requires first argument to be an interger")));
    return scope.Close(Undefined());
  }
  x=args[0]->Int32Value();
  
  if(!args[1]->IsNumber()){
    ThrowException(Exception::TypeError(String::New("ReadTile requires second argument to be an interger")));
    return scope.Close(Undefined());
  }
  y=args[1]->Int32Value();
  
  if(args[2]->IsNumber()){
    z=args[2]->Int32Value();
  }else{
    Local<Object> tmp=args[2]->ToObject();
    if(oinTypeDesc::constructor->HasInstance(tmp))
      oinTD=ObjectWrap::Unwrap<oinTypeDesc>(tmp);
  }
  
  if(args.Length()==4){
    Local<Object> tmp=args[3]->ToObject();
    if(oinTypeDesc::constructor->HasInstance(tmp))
      oinTD=ObjectWrap::Unwrap<oinTypeDesc>(tmp);
  }
  
  if(!oinTD){
    ThrowException(Exception::TypeError(String::New("ReadTile requires TypeDesc to be supplied")));
    return scope.Close(Undefined());
  }
  
  spec=input->img->spec();
  //Problem might occure if a tile is a cube for volumetic images
  size=spec.tile_width*spec.tile_height*spec.nchannels*oinTD->td.size();
  Buffer* tile=Buffer::New(size);

  input->img->read_tile(x,y,z,oinTD->td,Buffer::Data(tile));
  
  return scope.Close(attachBuffer(tile));
}

v8::Handle<v8::Value> oinInput::ReadImage(const v8::Arguments& args){
  HandleScope scope;

  size_t size;
  OpenImageIO::ImageSpec spec;
  oinTypeDesc* oinTD;

  oinInput* input=ObjectWrap::Unwrap<oinInput>(args.This());

  Local<Object> tmp=args[0]->ToObject();
  if(oinTypeDesc::constructor->HasInstance(tmp))
    oinTD=ObjectWrap::Unwrap<oinTypeDesc>(tmp);
  
  spec=input->img->spec();

  
  size=spec.width*spec.height*spec.depth*spec.nchannels*oinTD->td.size();
  Buffer* img=Buffer::New(size);

  input->img->read_image(oinTD->td,Buffer::Data(img));

  return scope.Close(attachBuffer(img));
}

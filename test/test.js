var nbitmap=require("node-bitmap");
var assert=require("assert");
var should=require("should");

var oiio=require("../build/Release/OpenImageNode");
describe("File Open",function(){
  it("Input",function(){
    var img=new oiio.Input("test/images/checker256-RGB8.png");
    should(img).be.ok;
  });
  it("FormatName",function(){
    var img=new oiio.Input("test/images/checker256-RGB8.png");
    should(img.FormatName()).equal("png");
  });
  it("CurrentSubimage",function(){
    var img=new oiio.Input("test/images/checker256-RGB8.png");
    should(img.CurrentSubimage()).equal(0);
  });
  describe("Spec",function(){
    var img=new oiio.Input("test/images/checker256-RGB8.png");
    it("Width",function(){should(img.Spec.Width).equal(256);});
    it("Height",function(){should(img.Spec.Width).equal(256);});
    
    it("DisplayWidth",function(){should(img.Spec.DisplayWidth).equal(256);});
    it("DisplayHeight",function(){should(img.Spec.DisplayHeight).equal(256);});
    
    it("ChannelCount",function(){should(img.Spec.ChannelCount).equal(3);});
  });
  describe("TypeDesc",function(){
    var img=new oiio.Input("test/images/checker256-RGB8.png");
    img.Spec.TypeDesc.Size
  
  });
});
